var cOwner,
	sType;
sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"ZMM_SLOW_OR_NON_MOVING/model/models"
], function(UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("ZMM_SLOW_OR_NON_MOVING.Component", {

	
			metadata: {
				manifest: "json"
			},
		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			debugger;
			cOwner = this;
			try{
				 sType = cOwner.getComponentData().startupParameters["TileName"][0];
				}catch(e){
					sType = "";
				}
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			//Set Right to Left
			sap.ui.getCore().getConfiguration().setRTL(true);
			//Set HE language
			sap.ui.getCore().getConfiguration().setLanguage("iw_IL");
			//create json model
			this.setModel(models.createJSONModel(), "JSON");
			
				//busyIndicator
			this.getModel().attachBatchRequestSent(function(){
				sap.ui.core.BusyIndicator.show();
			}).attachBatchRequestCompleted(function(event){
				sap.ui.core.BusyIndicator.hide();
			});
			//this.setAppTitle();
		}
		//app title 
		// 	setAppTitle: function() {
		// 		const sTitle = this.getModel("i18n").getProperty(sType);
		// 		this.getService("ShellUIService").then(
		// 		function(oService) {
		// 			oService.setTitle(sTitle);
		// 		},
		// 		function(oError) {
		// 			// jQuery.sap.log.error("Cannot get ShellUIService", oError, "my.app.Component");
		// 		}
		// 	);
		// }
	});
});