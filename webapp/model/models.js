/* global cOwner : true*/
sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device",
	"sap/ui/model/Filter"
], function(JSONModel, Device,Filter) {
	"use strict";
 
	return {

		createDeviceModel: function() {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},
			createJSONModel: function () {
			var oModel = new sap.ui.model.json.JSONModel({
				Filters:{}
			});
			oModel.setSizeLimit(1000);
			return oModel;
		},
		loadTableCount: function(){
			debugger;
			
			return new Promise(function(resolve, reject) {
				cOwner.getModel("ODATA").read("/zmm_cds_slow_stock_lgort/$count", {
					success: function(data) {
						debugger;
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
		},
		loadTableData: function(){
			debugger;
			var fData = cOwner.getModel("JSON").getData().Filters;
			var aFilters = [];
			if (fData.Matnr && fData.Matnr.length) {
				var aMatnr =[];
					for (const element of fData.Matnr) {
						aMatnr.push(new sap.ui.model.Filter("Material", "EQ", element.key));
					}
				aFilters.push(new Filter(aMatnr,false));
			}
			if (fData.Lgort && fData.Lgort.length) {
				var aLgort = [];
				for (const element of fData.Lgort) {
					aLgort.push(new sap.ui.model.Filter("lgort", "EQ", element.key));
				}
				aFilters.push(new Filter(aLgort, false));
			}	
			if (fData.Plant && fData.Plant.length) {
				var aPlant = [];
				for (const element of fData.Plant) {
					aPlant.push(new sap.ui.model.Filter("Plant", "EQ", element.key));
				}
				aFilters.push(new Filter(aPlant, false));
			}		
			return new Promise(function(resolve, reject) {
				cOwner.getModel("ODATA").read("/zmm_cds_slow_stock_lgort", {
					filters: aFilters,
					success: function(data) {
						debugger;
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
		}

	};
});