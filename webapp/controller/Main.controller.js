/* global cOwner : true*/
var initView = true;
sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"ZMM_SLOW_OR_NON_MOVING/model/models",
	"sap/ui/export/Spreadsheet",
	"sap/ui/model/Filter",
	"sap/ui/model/Sorter",
	'sap/ui/table/TablePersoController',
	"sap/m/MessageBox",
	"ZMM_SLOW_OR_NON_MOVING/model/formatter"
], function (Controller, models, Spreadsheet, Filter, Sorter, TablePersoController, MessageBox, formatter) {
	"use strict";

	return Controller.extend("ZMM_SLOW_OR_NON_MOVING.controller.Main", {
		formatter: formatter,
		onInit: function () {
			debugger;
			cOwner._MainController = this;
			cOwner.getModel("JSON").setProperty("/App", sType);
			try {
				var App = cOwner.getComponentData().startupParameters["App"][0];
				cOwner.getModel("JSON").setProperty("/App", App);
			} catch (e) {
				var App = "";
			}		
			this._oGlobalFilter = null;
			this._oPriceFilter = null;
			// cOwner._MainController.GetData();
			var oSmartTable = this.getView().byId("LineItemsSmartTable");
			// oSmartTable.getAggregation("items").sort[(new Sorter({ path: 'consumption_lst_date', descending: true }))];
			//aSorters.push(new Sorter({ path: sPath, descending: bDescending }));

			// oSmartTable.applyVariant({
			// 	sort: {
			// 		sortItems: [{
			// 			columnKey: "consumption_lst_date",
			// 			operation: "Ascending"
			// 		}
			// 		]
			// 	}
			// });




		},
		onAfterRendering: function () {
			debugger;
			try {
				this.oPersonalizationService = sap.ushell.Container.getService("Personalization");

				//const sItem = sap.ushell.services.AppConfiguration.getCurrentApplication().applicationDependencies.name;
				var oPersId = {
					container: "StockValuePersonalisation", //any
					item: 'StockValue' //any- I have used the table name 
				};
				var oScope = {
					keyCategory: this.oPersonalizationService.constants.keyCategory.FIXED_KEY,
					writeFrequency: this.oPersonalizationService.constants.writeFrequency.LOW,
					clientStorageAllowed: true
				};

				// Get a Personalizer
				var oPersonalizer = this.oPersonalizationService.getPersonalizer(oPersId, oScope, cOwner);

				this._oTPC = new TablePersoController({
					table: this.getView().byId("dataTable"),
					componentName: "StockValue",
					persoService: oPersonalizer
				});
			} catch (e) { }

		},
		onBeforeRebindTable: function (oEvent) {
			debugger;
			var mBindingParams = oEvent.getParameter("bindingParams");
			var model = cOwner.getModel("JSON"),
				fData = model.getProperty("/Filters"),
				aSort = mBindingParams.sorter.length > 0 ? mBindingParams.sorter[0] : '';
			if (fData.Matnr && fData.Matnr.length) {
				var aMatnr = [];
				for (const element of fData.Matnr) {
					aMatnr.push(new sap.ui.model.Filter("Material", "EQ", element.key));
				}
				mBindingParams.filters.push(new Filter(aMatnr, false));
			}
			if (fData.Lgort && fData.Lgort.length) {
				var aLgort = [];
				for (const element of fData.Lgort) {
					aLgort.push(new sap.ui.model.Filter("lgort", "EQ", element.key));
				}
				mBindingParams.filters.push(new Filter(aLgort, false));
			}
			if (fData.Plant && fData.Plant.length) {
				var aPlant = [];
				for (const element of fData.Plant) {
					aPlant.push(new sap.ui.model.Filter("Plant", "EQ", element.key));
				}
				mBindingParams.filters.push(new Filter(aPlant, false));
			}
			// if (initView) {
				// to apply the sort
				// mBindingParams.sorter = [new sap.ui.model.Sorter({ path: "consumption_lst_date", descending: true })];
				// to short the sorted column in P13N dialog
				var oSmartTable = oEvent.getSource();
				if(!aSort){
					mBindingParams.sorter = [new sap.ui.model.Sorter({ path: "consumption_lst_date", descending: true })];
					oSmartTable.applyVariant({
					sort: {
						sortItems: [{
							columnKey: "consumption_lst_date",
							operation: "Descending"
						}]
					}
				});}
				// to prevent applying the initial sort all times 
				// initView = false;
			// }

			// mBindingParams.sorter = [(new Sorter({ path: 'consumption_lst_date', descending: true }))];
			// oSmartTable.sort[(new Sorter({ path: 'consumption_lst_date', descending: true }))];
			// var mBindingParams = oEvent.getParameter("bindingParams");
			// var oSmtFilter = this.getView().byId("smartFilterBar");
			// var oComboBox = oSmtFilter.getControlByKey("Material");
			// var aCountKeys = oComboBox.getSelectedKeys();
			// var newFilter = new Filter("Count", FilterOperator.EQ, aCountKeys);
			// if (aCountKeys.length > 0) {
			// 	mBindingParams.filters.push(newFilter);
			// }
			// var sVariant = cOwner.getModel("JSON").getProperty("/variantId");
			// cOwner._MainController.getView().byId("smartFilterBar").setCurrentVariantId(sVariant);


		},
		setVariant: function (oEvent) {
			debugger;
			try{
			var App = cOwner.getModel("JSON").getProperty("/App");
			var smartFilterBar = cOwner._MainController.getView().byId("smartFilterBar");
			var smartTable = cOwner._MainController.getView().byId("LineItemsSmartTable");
			var DefaultVariant = smartFilterBar.getVariantManagement().getDefaultVariantKey();
			if (DefaultVariant === '*standard*') {
				var items = smartFilterBar.getVariantManagement().getVariantItems();
				for (var i = 0; i < items.length; i++) {
					if (App === 'Purchaser' && items[i].getProperty("text") === 'קניינים' || App === 'Pharm' && items[i].getProperty("text") === 'רוקחים') {
						var key = items[i].getProperty("key");
						setTimeout(function () {smartFilterBar.getVariantManagement().setDefaultVariantKey(key);
							smartFilterBar.setCurrentVariantId(key);
							smartTable.rebindTable()}, 1000);
					}
				}
			}
		}catch(e){}
		},
		onClear: function (oEvent) {
			debugger;
			cOwner.getModel("JSON").setProperty("/Filters", []);
		},
		GetData: function () {
			debugger;

			models.loadTableCount().then(function (data) {
				debugger;
				let aStorageLocation = [];
				// for (var i = 0; i < data.results.length; i++) {
				// 	if(!!data.results[i].consumption_lst_date){
				// 		var date = data.results[i].consumption_lst_date;
				// 		data.results[i].consumption_lst_date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
				// 	}
				// 	data.results[i].MaterialInt = parseInt(data.results[i].Material);
				// 	data.results[i].stkqtyInt = parseInt(data.results[i].stkqty);	
				// 	data.results[i].stk_valueInt = parseInt(data.results[i].stk_value);			
				// 	data.results[i].consumption_daysString = data.results[i].consumption_days.toString();
				// 	data.results[i].matdoc_daysString = data.results[i].matdoc_days.toString();

				// }
				cOwner.getModel("JSON").setProperty("/count", cOwner._MainController.addComma(data));
				// cOwner.getModel("JSON").setProperty("/orders", data.results);
			}).catch(function (error) {
				try {
					MessageBox.error(JSON.parse(error.responseText).error.message.value);
				} catch (e) {
					MessageBox.error(JSON.stringify(error));
				}
				console.log(error);
			});

		},
		removeDuplicates: function (array, key) {
			let lookup = new Set();
			return array.filter(obj => !lookup.has(obj[key]) && lookup.add(obj[key]));
		},
		fixDate: function (oDate) {
			var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "dd.MM.yyyy"
			});
			var sNewDate = dateFormat.format(oDate);
			return sNewDate;
		},
		addComma: function (value) {
			if (value) {
				value = value.toString();
				return value.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}
			return value;
		},
		_filter: function () {
			var oFilter = null;
			// if (this._oGlobalFilter && this._oPriceFilter) {o
			// 	oFilter = new sap.ui.model.Filter([this._oGlbalFilter, this._oPriceFilter], true);
			// } else if (this._oGlobalFilter) {
			// 	oFilter = this._oGlobalFilter;
			// } else if (this._oPriceFilter) {
			// 	oFilter = this._oPriceFilter;
			// }


			// cOwner.getModel("JSON").setProperty("/count", cOwner._MainController.byId("dataTable").getBinding("rows").getLength());
		},

		filterGlobally: function (oEvent) {
			this._oGlobalFilter = null;
			var oFilters = [];
			var model = cOwner.getModel("JSON"),
				fData = model.getProperty("/Filters");
			if (fData.Matnr && fData.Matnr.length) {
				var aMatnr = [];
				for (const element of fData.Matnr) {
					aMatnr.push(new sap.ui.model.Filter("Material", "EQ", element.key));
				}
				oFilters.push(new Filter(aMatnr, false));
			}
			if (fData.Lgort && fData.Lgort.length) {
				var aLgort = [];
				for (const element of fData.Lgort) {
					aLgort.push(new sap.ui.model.Filter("lgort", "EQ", element.key));
				}
				oFilters.push(new Filter(aLgort, false));
			}
			if (fData.Plant && fData.Plant.length) {
				var aPlant = [];
				for (const element of fData.Plant) {
					aPlant.push(new sap.ui.model.Filter("Plant", "EQ", element.key));
				}
				oFilters.push(new Filter(aPlant, false));
			}
			if (!!fData.DateString) { // only to
				oFilters.push(new sap.ui.model.Filter("consumption_lst_date", "EQ", fData.DateString));
			}

			if (oFilters.length !== 0) {
				this._oGlobalFilter = new Filter(oFilters, true);
			}

			cOwner._MainController.byId("dataTable").getBinding("rows").filter(oFilters);
		},

		clearAllFilters: function (oEvent) {
			var oFilter = [];
			var oTable = cOwner._MainController.byId("dataTable");
			oTable.getBinding("rows").filter(oFilter);
			// var aColumns = oTable.getColumns();
			// for (var i = 0; i < aColumns.length; i++) {
			// 	oTable.filter(aColumns[i], null);
			// 	oTable.sort(aColumns[i], null);
			// }
			cOwner.getModel("JSON").setProperty("/Filters", []);
		},

		getTableFilters: function () {
			var oTable = cOwner._MainController.byId("dataTable");
			var oColumns = oTable.getColumns();
			var aFilters = [];
			for (var i = 0; i < oColumns.length; i++) {
				var sValue = oColumns[i].getFilterValue();
				if (sValue) {
					aFilters.push({
						value: sValue,
						property: oColumns[i].getFilterProperty()
					});
				}
			}
			return aFilters;
		},
		setTableFilters: function (tableFilters) {
			var oTable = cOwner._MainController.byId("dataTable");
			var oColumns = oTable.getColumns();
			for (var i = 0; i < oColumns.length; i++) {
				for (var j = 0; j < tableFilters.length; j++) {
					if (tableFilters[j].property === oColumns[i].getFilterProperty()) {
						oColumns[i].filter(tableFilters[j].value);
						continue;
					}
				}
			}
		},
		createColumnConfig: function () {
			var aCols = cOwner._MainController.byId("dataTable").getColumns();
			var property = cOwner._MainController.byId("dataTable").getBinding("rows").oEntityType.property;
			var aColumns = [];
			for (var i = 0; i < aCols.length - 1; i++) {
				if (cOwner._MainController.byId("dataTable").getColumns()[i].getVisible()) {
					aColumns.push({
						label: aCols[i].getLabel().getProperty("text") || " ",
						property: property[i].name,
						width: '15rem'
					});
				}
			}
			return aColumns;

		},
		onExport: function () {
			var aCols, aItems, oSettings, oSheet;
			aCols = this.createColumnConfig();
			aItems = cOwner.getModel("JSON");
			var aSelectedModel = [];
			// var aSelectedModel = cOwner._MainController.byId("dataTable").getBinding("rows");
			// var aKeys = cOwner._MainController.byId("dataTable").getBinding("rows").aKeys;
			models.loadTableData().then(function (data) {
				aSelectedModel = data.results;
				oSettings = {
					workbook: {
						columns: aCols
					},
					dataSource: aSelectedModel,
					fileName: cOwner.getModel("JSON").getProperty("/ExportName")
				};

				oSheet = new Spreadsheet(oSettings);
				oSheet.build()
					.then(function () { });
			}).catch(function (error) {

			});
			// for (var i = 0; i<aKeys.length; i++){
			// 	var oRow = this.getOwnerComponent().getModel("ODATA").getProperty("/" + aKeys[i]);
			// 	// oRow.consumption_lst_date = !!oRow.consumption_lst_date ? this.fixDate(oRow.consumption_lst_date) : oRow.consumption_lst_date;
			// 	// oRow.matdoc_lst_date = !!oRow.matdoc_lst_date ? this.fixDate(oRow.matdoc_lst_date) : oRow.matdoc_lst_date;
			// 	aSelectedModel.push(oRow);
			// }
			// var aSelectedModel = cOwner._MainController.byId("dataTable").getBinding("rows").oList;


		},
		showDialog: function (sDialogName) {
			if (!this[sDialogName]) {
				this[sDialogName] = sap.ui.xmlfragment("ZMM_SLOW_OR_NON_MOVING.view.fragments." + sDialogName, this);
				this.getView().addDependent(this[sDialogName]);
			}
			this[sDialogName].open();
		},
		closeDialog: function (sDialogName) {
			this[sDialogName]._dialog.close();
			// this[sDialogName].destroy();
			// delete(this[sDialogName]);
		},
		onConfirm: function (oEvent, sType) {
			const aSelected = oEvent.getParameter("selectedContexts");
			const model = cOwner.getModel("JSON");
			var aTokens = this.CreateToken(aSelected, sType, "SH");
			// for (var i = 0; i < aTokens.length; i++) {
			// 	var object = aSelected[i].getBindingContext("SH").getObject();
			// 	aTokens[i].StorageLocation = object.Lgort;
			// }
			model.setProperty("/Filters/" + sType, aTokens);
			this.closeDialog(oEvent);
		},
		search: function (oEvent, sType) {
			const value = oEvent.getParameter("value");
			var oFilter;
			if (sType === 'Lgort') {
				oFilter = new Filter({
					filters: [new Filter("Lgort", "Contains", value.toUpperCase()),
					new Filter("Lgobe", "Contains", value.toUpperCase())
					],
					and: false
				});
			}
			else if (sType === 'Matnr') {
				oFilter = new Filter({
					filters: [new Filter("Matnr", "Contains", value.toUpperCase()),
					new Filter("Maktx", "Contains", value.toUpperCase())
					],
					and: false
				});
			}
			else if (sType === 'Plant') {
				oFilter = new Filter({
					filters: [new Filter("Werks", "Contains", value.toUpperCase()),
					new Filter("Name1", "Contains", value.toUpperCase())
					],
					and: false
				});
			}
			oEvent.getSource().getBinding("items").filter([oFilter]);
		},
		CreateToken: function (aSelected, sKey, sModel) {
			var aTokens = [];
			for (var i = 0; i < aSelected.length; i++) {
				var object = this.getOwnerComponent().getModel(sModel).getProperty(aSelected[i].sPath);
				if (sKey === 'Lgort') {
					aTokens.push({
						text: object['Lgobe'],
						key: object['Lgort']
					});
				}
				else if (sKey === 'Matnr') {
					aTokens.push({
						text: object['Maktx'],
						key: object['Matnr']
					});
				}
				else if (sKey === 'Plant') {
					aTokens.push({
						text: object['Name1'],
						key: object['Werks']
					});
				}
			}
			return aTokens;

		},
		onDeleteToken: function (oEvent, sPath) {
			debugger;
			const sKey = oEvent.getParameter("token").getBindingContext("JSON").getObject().key;
			const model = cOwner.getModel("JSON");
			var aTokens = model.getProperty("/Filters/" + sPath);
			const index = this.findIndexToDelete(aTokens, sKey);
			aTokens.splice(index, 1);
			model.setProperty("/Filters/" + sPath, aTokens);
			// this._filter();
		},
		findIndexToDelete: function (aTokens, sKey) {
			for (var i = 0; i < aTokens.length; i++) {
				if (sKey === aTokens[i].key) {
					return i;
				}
			}
		},

		onPersoButtonPressed: function (oEvent) {
			cOwner._MainController._oTPC.openDialog();
		},
		GoToTransaction: function (sMaterial, sPlant, sStorageLocation) {
			var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
			var semamticActionObj = "ZMM_SLOW_MB51-display";
			var oParams = {
				IvMaterial: sMaterial,
				IvPlant: sPlant,
				IvStorageLocation: sStorageLocation
			};
			// this.setAppState(oCrossAppNavigator);
			oCrossAppNavigator.isIntentSupported([semamticActionObj])
				.done(function (aResponses) {
					if (aResponses[semamticActionObj].supported === true) {
						var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
							target: {
								semanticObject: "ZMM_SLOW_MB51",
								action: "display"
							},
							params: oParams
						})) || "";
						oCrossAppNavigator.toExternal({
							target: {
								shellHash: hash
							}
						});
					}

				})
				.fail(function () { });
		}
	});
});